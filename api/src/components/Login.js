import React, { useContext, useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import UserProfile from './UserProfile';
import { useHistory } from "react-router-dom";
import { loginUser } from "../controllers/appController";
import Alert from '../components/alert';
import AppContext from '../context/index';


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="#">
        Observatorio Pyme
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

let data;

export default function Login(props) {
  const classes = useStyles();
  let history = useHistory();
  let user;
  const [showLoginError, setShowLoginError] = React.useState(false);
  const [showInactiveUserLoginError, setShowInactiveUserLoginError] = React.useState(false);
  const { loggedIn, setLoggedIn } = useContext(AppContext);

  const SignIn = e => {
    e.preventDefault();
    var user = document.getElementById('user').value;
    var pass = document.getElementById('password').value;
    loginUserApi(user, pass);

  };

  /*
  useEffect(() => {
    console.log('context here: ', loggedIn);
}, [loggedIn]);
*/

  /* 
   if (localStorage.getItem("SessionToken") != '' && localStorage.getItem("Status") == 'ACTIVO') {
     history.push("/listadoEncuestas");
   }
   */

  const loginUserApi = async function (user, pass) {
    let data = await loginUser(user, pass);
    //console.log("data: " + data);
    if (data != undefined && data.length !== 0 && data.status == 200) {
      //console.log(data);

      localStorage.setItem("ID", data.loggedInUser.user.id);
      localStorage.setItem("Name", data.loggedInUser.user.nombre);
      localStorage.setItem("UserName", data.loggedInUser.user.email);
      localStorage.setItem("SessionToken", data.loggedInUser.token);
      localStorage.setItem("Profile", data.loggedInUser.user.rol);
      localStorage.setItem("Status", data.loggedInUser.user.estado);

      if (data.loggedInUser.user.estado == 'ACTIVO') {
        setLoggedIn(true);
        props.onChange(true);
        if (data.loggedInUser.user.rol === 'Administrador') {
          history.push("/admin");
          //window.location.reload();
        } else {
          history.push("/listadoEncuestas");
          //window.location.reload();
        }
      }
      else {
        setShowInactiveUserLoginError(true);
      }
    }
    else {
      setShowLoginError(true);
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Autenticación
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="user"
            label="Nombre de Usuario"
            name="user"
            autoComplete="user"
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Contraseña"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={SignIn}
            user={user}
          >
            Ingresar
          </Button>
          {
            (showLoginError)
              ?
              (
                <Alert CompProps='Usuario o contraseña inválido' type='error' />
              )
              : (showInactiveUserLoginError)
                ?
                (
                  <Alert CompProps='Usuario inactivo' type='info' />
                )
                :
                (
                  <React.Fragment />
                )
          }

        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}