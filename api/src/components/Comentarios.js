import React, { useEffect, useState } from 'react';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';


export default function Comentarios(props) {

    const [open, setOpen] = React.useState(false);

    return (
        <React.Fragment>
            {/* {console.log(props.comentarios)} */}

            <React.Fragment>
                <Box margin={2}>
                    <Typography variant="h7" gutterBottom component="div">
                        Comentarios
                <IconButton id={"flecha" + props.pregunta} aria-label="ver detalles" size="small" onClick={() => setOpen(!open)}>
                            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                        </IconButton>
                    </Typography>
                </Box>
                <TableRow>
                    <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                        <Collapse in={open} timeout="auto" unmountOnExit>
                            <Table size="small" aria-label="comentarios">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Fecha</TableCell>
                                        <TableCell>Comentario</TableCell>
                                        <TableCell>Usuario</TableCell>
                                        <TableCell>Rol</TableCell>
                                    </TableRow>
                                </TableHead>

                                <TableBody>
                                    {props.comentarios.map((historyRow) => (
                                        <TableRow key={historyRow.idComentario}>
                                            <TableCell>{historyRow.fecha}</TableCell>
                                            <TableCell>{historyRow.comentario}</TableCell>
                                            <TableCell>{historyRow.usuario}</TableCell>
                                            <TableCell>{historyRow.rol}</TableCell>
                                        </TableRow>
                                    ))
                                    }
                                </TableBody>
                            </Table>
                        </Collapse>
                    </TableCell>
                </TableRow>
            </React.Fragment>

        </React.Fragment>
    )
}



