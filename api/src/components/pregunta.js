import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import MultilineText from './MultilineText'
import Botones from './botones';
import Comentarios from './Comentarios';
import RenderRta from './campoRta'


const useRowStyles = makeStyles({
    root: {
        '& > *': {
            borderBottom: 'unset',
        },
    },
    Button: {
        margin: 200,
    },
});

function Row(props) {
    const { row } = props;
    const [textRechazar, setTextRechazar] = React.useState('');
    const [rta, setRta] = React.useState('');
    const multiple = [];
    const [respuestaMultiple = [], setRespuestasMultiple] = React.useState('');

    const callbackText = (dataText) => {
        setTextRechazar(dataText)
    }

    const callbackRta = (dataText) => {
        if (row.type !== "multiple") {
            setRta(dataText)
            console.log(dataText);
            console.log(row.type);
        } else {
            if (multiple.length === 0) {
                multiple.push(dataText);
            }
            else {
                var band = 0;
                for (var i = 0; i < multiple.length; i++) {
                    if (multiple[i].respuesta === dataText.respuesta && band === 0) {
                        multiple[i].estado = dataText.estado;
                        band = 1
                    }
                }
                if (band === 0) {
                    multiple.push(dataText);
                }
            }
        }

    }


    return (
        <TableRow>
            <TableCell>
                <RenderRta id="respuestaOriginal"
                    tipo={row.type}
                    rta={row.rtaOriginal}
                    valores={row.valoresPosibles}
                    label="Respuesta Original"
                    soloLectura="true"
                >
                </RenderRta>
            </TableCell>
            {
                (localStorage.getItem("Profile") === 'Analista')
                    ?
                    (
                        <TableCell>
                            <RenderRta id="respuestaActualizada"
                                tipo={row.type}
                                rta={row.rtaActualizada}
                                valores={row.valoresPosibles}
                                label="Respuesta Corregida"
                                soloLectura={row.status !== 'Aprobada' ? "false" : "true"}
                                callbackFromParent={callbackRta}
                            >
                            </RenderRta>
                        </TableCell>
                    )
                    :
                    (
                        ((row.rtaActualizada !== row.rtaOriginal) && (localStorage.getItem("Profile") === 'Aprobador'))
                            ?
                            (
                                <TableCell>
                                    <RenderRta id="respuestaActualizada"
                                        tipo={row.type}
                                        rta={row.rtaActualizada}
                                        valores={row.valoresPosibles}
                                        label="Respuesta Corregida"
                                        soloLectura="true"

                                    >
                                    </RenderRta>
                                </TableCell>
                            )
                            :
                            (
                                <TableCell></TableCell>
                            )
                    )
            }

            {

                (
                    props.statusEncuesta === 'Pendiente Aprobador' ||
                    (props.statusEncuesta === 'Pendiente Analista' && localStorage.getItem("Profile") === 'Analista')
                )
                    ?
                    (
                        <React.Fragment>
                            <TableCell>
                                <form noValidate autoComplete="off">
                                    <MultilineText id={"txt" + row.idPregunta}
                                        key={row.numeroPregunta}
                                        callbackFromParent={callbackText}
                                        label="Comentarios"
                                        variant="outlined"
                                        multiline
                                        rows={4}
                                        visible={(row.status === 'Aprobada' && localStorage.getItem("Profile") === 'Analista') ? false : true}
                                        InputProps={{ disabled: false, }}
                                    />
                                </form>
                            </TableCell>
                            <TableCell>
                            </TableCell>
                            <TableCell>
                                <Botones
                                    state={{
                                        idPregunta: row.idPregunta,
                                        texto: textRechazar,
                                        rtaActualizada: rta,
                                        status: row.status,
                                        type: row.type,
                                        rtaMultiple: multiple
                                    }}
                                    onChange={props.onChange}
                                    reload={props.reload}
                                >
                                </Botones>
                            </TableCell>
                        </React.Fragment>
                    )
                    :
                    (
                        <React.Fragment>
                        </React.Fragment>
                    )
            }
        </TableRow>
    )
}


export default function MostrarPregunta(props) {
    const row = props.unaPregunta;
    var keyId = "keyComentario"
    const comentarios =row.history.comentariosRelacionados;
    const statusEncuesta = props.statusEncuesta;
    const statusPregunta = row.status;

    return (
        <React.Fragment>
            {
                (statusPregunta === 'Pendiente Validación')
                    ?
                    (
                        <React.Fragment>
                            <Box margin={1} width={1800}>
                                <div className="alert alert-warning" id={"div" + row.idPregunta}>
                                    <div class="alert-content">
                                        <h2 class="alert-title">
                                            Pendiente Validación
                                        </h2>
                                        <Typography variant="h5" gutterBottom component="div">
                                            {row.numeroPregunta}: {row.titulo}
                                        </Typography>
                                        <Row
                                            key={row.idPregunta}
                                            row={row}
                                            statusEncuesta={statusEncuesta}
                                            onChange={props.onChange}
                                            reload={props.reload}
                                        />
                                    </div>
                                </div>
                            </Box>
                        </React.Fragment>
                    )
                    :
                    (
                        (statusPregunta === 'Aprobada')
                            ?
                            (
                                <React.Fragment>
                                    <Box margin={1} width={1800}>
                                        <div className="alert alert-success" id={"div" + row.idPregunta}>
                                            <div class="alert-content">
                                                <h2 class="alert-title">
                                                    Aprobada
                                                        </h2>
                                                <Typography variant="h5" gutterBottom component="div">
                                                    {row.numeroPregunta}: {row.titulo}
                                                </Typography>
                                                <Row
                                                    key={row.idPregunta}
                                                    row={row}
                                                    statusEncuesta={statusEncuesta}
                                                    onChange={props.onChange}
                                                    reload={props.reload}
                                                />
                                            </div>
                                        </div>
                                    </Box>
                                </React.Fragment>
                            )
                            :
                            (
                                (statusPregunta === 'Pendiente Analista')
                                    ?
                                    (
                                        <React.Fragment>
                                            <Box margin={1} width={1800}>
                                                <div className="alert alert-error" id={"div" + row.idPregunta}>
                                                    <div class="alert-content">
                                                        <h2 class="alert-title">
                                                            Rechazada
                                                        </h2>
                                                        <Typography variant="h5" gutterBottom component="div">
                                                            {row.numeroPregunta}: {row.titulo}
                                                        </Typography>
                                                        <Row
                                                            key={row.idPregunta}
                                                            row={row}
                                                            statusEncuesta={statusEncuesta}
                                                            onChange={props.onChange}
                                                            reload={props.reload}
                                                        />
                                                    </div>
                                                </div>
                                            </Box>
                                        </React.Fragment>
                                    )
                                    :
                                    (
                                        <React.Fragment>
                                        </React.Fragment>
                                    )
                            )

                    )

            }

            {

                (comentarios.length === 0)
                    ?
                    (
                        <Box margin={1}>
                            <Typography variant="h7" gutterBottom component="div">
                                Sin comentarios
                                            </Typography>
                        </Box>
                    )
                    :
                    (
                        <React.Fragment>
                            <Comentarios
                                key={keyId}
                                id={"comentario" + row.idPregunta}
                                comentarios={comentarios}
                                pregunta={row.idPregunta}>
                            </Comentarios>
                        </React.Fragment>
                    )
            }
        </React.Fragment>
    );
}


