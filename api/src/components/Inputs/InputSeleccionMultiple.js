import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    formControl: {
        margin: theme.spacing(3),
    },
}));


export default function InputSeleccionMultiple(props) {

    const classes = useStyles();
    const readOnly = props.CompProps.soloLectura == 'true' ? true : false;
    const [state, setState] = React.useState(props.CompProps.rta);
    const [respuesta, setRespuesta] = React.useState(props.CompProps);

    const handleChange = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
        let respuestaMc = {
            respuesta: event.target.name,
            estado: event.target.checked
        }
        respuesta.callbackFromParent(respuestaMc);
        console.log(respuestaMc);
    };

    return (
        <div className={classes.root}>
            <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend">{props.CompProps.label}</FormLabel>
                <FormGroup>
                    {
                        props.CompProps.valores.map((valor, i) => {
                            return (
                                <FormControlLabel
                                    control={<Checkbox checked={state[valor]} disabled={readOnly} onChange={handleChange} name={valor} />}
                                    label={valor}
                                />
                            );
                        })
                    }
                </FormGroup>
            </FormControl>
        </div>
    );
}