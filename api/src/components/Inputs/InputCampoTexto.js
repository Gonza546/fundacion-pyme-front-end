import React from 'react';
import TextField from '@material-ui/core/TextField';

export default function InputCampoTexto(propiedades) {
    propiedades = propiedades.CompProps;

    const handleInputChange = (e) => {
        propiedades.callbackFromParent(e.target.value)
      }

    if (propiedades.soloLectura === 'true') {

        return (
            <form noValidate autoComplete="off">
                <TextField id="outlined-basic"
                    defaultValue={propiedades.rta}
                    variant="outlined"
                    label={propiedades.label}
                    InputProps={{ readOnly: true }}
                />
            </form>
        );
    }
    else {
        return (
            <form noValidate autoComplete="off">
                <TextField id="outlined-basic"
                    defaultValue={propiedades.rta}
                    variant="outlined"
                    label={propiedades.label}
                    onChange={handleInputChange}
                />
            </form>
        );
    }
}