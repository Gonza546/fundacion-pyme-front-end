import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import clsx from 'clsx';
import Radio from '@material-ui/core/Radio';
import UserProfile from '../UserProfile';

export default function InputSeleccionSimple(propiedades) {
    const [formData, setformData] = React.useState('');
    const [value, setValue] = React.useState('')
    propiedades = propiedades.CompProps;

    const handleInputChange = (e) => {
        propiedades.callbackFromParent(e.target.value)
    }

    //const handleChange=(e)=>{
    //const target=e.target
    //const valor=target.valor
    //const value=target.value
    //setformData({
    //    ...formData,
    //      [valor]:value
    //    })
    //  }

    const useStyles = makeStyles({
        root: {
            '&:hover': {
                backgroundColor: 'transparent',
            },
        },
        icon: {
            borderRadius: '50%',
            width: 16,
            height: 16,
            boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
            backgroundColor: '#f5f8fa',
            backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
            '$root.Mui-focusVisible &': {
                outline: '2px auto rgba(19,124,189,.6)',
                outlineOffset: 2,
            },
            'input:hover ~ &': {
                backgroundColor: '#ebf1f5',
            },
            'input:disabled ~ &': {
                boxShadow: 'none',
                background: 'rgba(206,217,224,.5)',
            },
        },
        checkedIcon: {
            backgroundColor: '#137cbd',
            backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
            '&:before': {
                display: 'block',
                width: 16,
                height: 16,
                backgroundImage: 'radial-gradient(#fff,#fff 28%,transparent 32%)',
                content: '""',
            },
            'input:hover ~ &': {
                backgroundColor: '#106ba3',
            },
        },
    });

    // Inspired by blueprintjs
    function StyledRadio(props) {
        const classes = useStyles();

        return (
            <Radio
                className={classes.root}
                disableRipple
                color="default"
                checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
                icon={<span className={classes.icon} />}
                {...props}
            />
        );
    }

    if (propiedades.soloLectura == 'true' | localStorage.getItem("Profile") == 'Aprobador') {
        return (
            <FormControl disabled component="fieldset">
                <FormLabel component="legend">{propiedades.label}</FormLabel>
                <RadioGroup defaultValue={propiedades.rta} aria-label="rta" name="customized-radios">
                    {
                        propiedades.valores.map(valor => {
                            return (
                                <FormControlLabel value={valor} control={<StyledRadio />} label={valor} />
                            )
                        }
                        )
                    }
                </RadioGroup>
            </FormControl>
        );

    }
    else {
        return (
            <FormControl component="fieldset">
                <FormLabel component="legend">{propiedades.label}</FormLabel>
                <RadioGroup defaultValue={propiedades.rta} aria-label="rta" name="customized-radios">
                    {
                        propiedades.valores.map(valor => {
                            return (
                                <FormControlLabel value={valor} control={<StyledRadio />} label={valor} onChange={handleInputChange} />
                            )
                        }
                        )
                    }
                </RadioGroup>
            </FormControl>
        );
    }
}