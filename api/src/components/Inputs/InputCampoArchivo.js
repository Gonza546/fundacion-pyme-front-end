import React from 'react';
import TextField from '@material-ui/core/TextField';
import InputBase from '@material-ui/core/InputBase';
import {BrowserRouter as Router,Link} from 'react-router-dom';
import Route from 'react-router-dom/Route';

export default function InputCampoArchivo(propiedades) {
    propiedades = propiedades.CompProps;

    const handleInputChange = (e) => {
        propiedades.callbackFromParent(e.target.value)
      }

    if (propiedades.soloLectura === 'true') {

        return (
            <form noValidate autoComplete="off">
                <Router>
                    <Link to={propiedades.rta}>Descargar Archivo</Link>
                </Router>

                <Route path={propiedades.rta} component={()=>{window.location=propiedades.rta;return null;}} ></Route>
            </form>
        );
    }
    else {
        return (
            <form noValidate autoComplete="off">
                <TextField id="outlined-basic"
                    defaultValue={propiedades.rta}
                    variant="outlined"
                    label={propiedades.label}
                    onChange={handleInputChange}
                />
            </form>
        );
    }
}