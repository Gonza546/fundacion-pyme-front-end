import React from 'react';
import TextField from '@material-ui/core/TextField';

export default function InputCampoNumerico(propiedades) {
    propiedades = propiedades.CompProps;

    const handleInputChange = (e) => {
        propiedades.callbackFromParent(e.target.value)
    }
    
    if (propiedades.soloLectura === 'true') {
        return (
            <TextField
                id="outlined-number"
                defaultValue={parseInt(propiedades.rta)}
                type="number"
                label={propiedades.label}
                InputProps={propiedades.soloLectura}
                InputProps={{
                    readOnly: true,
                    shrink: true,
                }}
                variant="outlined"
            />);
    }
    else {
        return (
            <TextField
                id="outlined-number"
                defaultValue={parseInt(propiedades.rta)}
                type="number"
                label={propiedades.label}
                InputLabelProps={{
                    shrink: true,
                }}
                variant="outlined"
                onChange={handleInputChange}
            />);

    }
}