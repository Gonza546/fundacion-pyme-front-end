var Filtro = (function () {
    var valorFiltrado = "Todo";//Default

    //Getters
    var getValorFiltrado = function () {
        return valorFiltrado;
    };

    ///Setters
    var setValorFiltrado = function (filt) {
        valorFiltrado = filt;
    };

    return {
        getValorFiltrado: getValorFiltrado,
        setValorFiltrado: setValorFiltrado,
    }
    
})();

export default Filtro;