import React, { useState, useEffect } from 'react';
import { DataGrid } from '@material-ui/data-grid';
import { useHistory } from "react-router-dom";
import UserProfile from './UserProfile';
import { getUsers, deleteUsers } from "../controllers/appController";
import { Grid } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';

const columns = [
    //  { field: 'id', headerName: 'ID', width: 70 },
    { field: 'fullName', headerName: 'Nombre', width: 250 },
    { field: 'email', headerName: 'Email', width: 250 },
    { field: 'status', headerName: 'Estado', width: 200 },
    { field: 'role', headerName: 'Rol', width: 200 },
];





export default function DataTable() {
    const [rows, setRows] = React.useState([]);
    const [dataLoaded, setDataLoaded] = React.useState(false);
    let data;
    let userDeletedData;
    let history = useHistory();

    if (localStorage.getItem("SessionToken") == '') {
        history.push("/")
      }

    // This gets called after every render
    useEffect(() => {
        getUSersApi();

    });

    function createData(i, nombre, mail, estado, rol) {
        return { id: i, fullName: nombre, email: mail, status: estado, role: rol };
    }


    const getUSersApi = async function () {
        var rowData = [];
        if (!dataLoaded) {
            data = await getUsers();

            if (data != undefined && data.usuario != undefined && data.status == 200) {
                data.usuario.map((user, i) => {
                    rowData.push(createData(i, user.nombre, user.email, user.estado, user.rol))
                })
                //console.log(rowData);
                setRows(rowData);
                setDataLoaded(true);
            }
        }
    }

    var usersSelected = [];
    const handleRowSelection = (e) => {
    };

    const handleSelectionChange = (e) => {
        usersSelected = [];
        e.rowIds.map((rowSelected) => {
            usersSelected.push(rows[rowSelected].email);
        })
        //console.log(usersSelected);
    };


    const deleteUsersApi = async function () {
        userDeletedData = await deleteUsers(usersSelected);

        if (userDeletedData != undefined && userDeletedData.length !== 0 && userDeletedData.status == 200) {
            usersSelected = [];
            setDataLoaded(!dataLoaded);
        }
    };

    const deleteU = () => {
        deleteUsersApi();
    }

    return (
        <React.Fragment>
            <Grid item xs={12} container justify="center" alignItems="center">

                <Button variant="contained" color="primary" onClick={() => { history.push("/admin") }}>
                    Alta de usuario
			</Button>

                <Button variant="contained" color="secondary" onClick={() => { history.push("/editarusuario") }}>
                    Lista de usuarios
			</Button>
            </Grid>
            <div style={{ height: 600, width: '100%' }}>
                <Tooltip title="Delete">
                    <IconButton aria-label="delete" onClick={deleteU}>
                        <DeleteIcon />
                    </IconButton>
                </Tooltip>
                <DataGrid
                    rows={rows}
                    columns={columns}
                    pageSize={10}
                    checkboxSelection
                    onRowSelected={handleRowSelection}
                    onSelectionChange={handleSelectionChange}
                    disableMultipleSelection
                    hideFooterPagination
                    hideFooterRowCount
                />
            </div>
        </React.Fragment>
    );
}