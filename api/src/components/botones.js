import React from 'react';
import Button from '@material-ui/core/Button';
import { useTheme } from '@material-ui/core/styles';
import CheckIcon from '@material-ui/icons/Check';
import { green } from '@material-ui/core/colors';
import {
  crearComentario,
  preguntaAprobada,
  rechazarPreguntaPoridPregunta,
  preguntaDevolver,
  actualizarRespuesta
}
  from '../controllers/appController';
import CloseIcon from '@material-ui/icons/Close';
import { red, blue } from '@material-ui/core/colors';
import SaveIcon from '@material-ui/icons/Save';


export default function BotonesAprobador(props) {
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  let data;
  let data2;
  var iduser = parseInt(localStorage.getItem("ID"));
  const statusPregunta = props.state.status;


  const updateRespuesta = async function () {
    if (props.state.type === "texto" || props.state.type === "simple") {
      var respuesta = {
        rtaActualizada: props.state.rtaActualizada,
        idPregunta: props.state.idPregunta
      }
    }
    else {

      if (props.state.type === "numerica") {
        var rtaNumerica = parseInt(props.state.rtaActualizada);
        var respuesta = {
          rtaActualizada: rtaNumerica,
          idPregunta: props.state.idPregunta
        }
      }
      else {
        for (var i = 0; i < props.state.rtaMultiple.length; i++) {
          var preparo;

          if (props.state.rtaMultiple[i].estado === true) {
            if (preparo === undefined) {
              preparo = props.state.rtaMultiple[i].respuesta;
            } else {
              preparo = preparo + "," + props.state.rtaMultiple[i].respuesta;
            }
          }
        }
        var respuesta = {
          rtaActualizada: preparo,
          idPregunta: props.state.idPregunta
        }
        console.log("muestro la respuesta", preparo);
        console.log("Muestro la prop: ", props.state.rtaMultiple);
      }
    }
    data = await actualizarRespuesta(respuesta);

  }


  const guardarComentario = async function () {
    if (JSON.stringify(props.state.texto) !== '""') {
      var envio = {
        idPregunta: props.state.idPregunta,
        idUsuario: iduser,
        comentario: props.state.texto,
      }
      crearComentario(envio);
      document.getElementById("txt" + props.state.idPregunta).value = "";
    }
  }

  const rechazarPregunta = async function () {
    guardarComentario();
    data2 = await rechazarPreguntaPoridPregunta(props.state.idPregunta);
    props.onChange(props.reload + 1);
  }

  const aprobarPregunta = async function () {
    guardarComentario();
    data = await preguntaAprobada(props.state.idPregunta);
    props.onChange(props.reload + 1);
  };

  const devolverPregunta = async function () {
    guardarComentario();
    updateRespuesta();
    data = await preguntaDevolver(props.state.idPregunta);
    props.onChange(props.reload + 1);
  };


  return (
    <React.Fragment>
      {
        (localStorage.getItem("Profile") === 'Aprobador')
          ?
          (
            (statusPregunta === 'Pendiente Validación')
              ?
              (
                <React.Fragment>
                  <Button key={props.idPregunta + "rechazar"} style={{ color: red[500] }}
                    onClick={rechazarPregunta}>
                    <CloseIcon fontSize="large"></CloseIcon>
                  </Button>

                  <span style={{ marginLeft: 20, marginRight: 20 }}>
                    <Button key={props.idPregunta + "aprobar"} style={{ color: green[500] }}
                      onClick={aprobarPregunta} >
                      <CheckIcon fontSize="large"></CheckIcon>
                    </Button>
                  </span>
                </React.Fragment>
              )
              :
              (
                (statusPregunta === 'Aprobada')
                  ?
                  (
                    <React.Fragment>
                      <Button
                        key={props.idPregunta + "rechazar"}
                        style={{ color: red[500] }}
                        onClick={rechazarPregunta}
                      >
                        <CloseIcon fontSize="large"></CloseIcon>
                      </Button>
                    </React.Fragment>
                  )
                  :
                  (
                    <React.Fragment>
                      <span style={{ marginLeft: 20, marginRight: 20 }}>
                        <Button
                          key={props.idPregunta + "aprobar"}
                          style={{ color: green[500] }}
                          onClick={aprobarPregunta}
                        >
                          <CheckIcon fontSize="large"></CheckIcon>
                        </Button>
                      </span>
                    </React.Fragment>
                  )
              )
          )
          :
          (
            (statusPregunta !== 'Aprobada')
              ?
              (
                <React.Fragment>
                  <Button
                    key={props.idPregunta + "guardar"}
                    style={{ color: blue[500] }}
                    onClick={devolverPregunta}
                  >
                    <SaveIcon fontSize="large"></SaveIcon>
                  </Button>
                </React.Fragment>
              )
              :
              (
                <React.Fragment>
                </React.Fragment>
              )
          )

      }
    </React.Fragment>
  );
}
