import React, { useContext, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import logo from '../assets/images/logo.png';
import { Link } from "react-router-dom";
import UserProfile from './UserProfile';
import AccountCircle from '@material-ui/icons/AccountCircle';
import SvgIcon from '@material-ui/core/SvgIcon';
import { Box } from '@material-ui/core';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { useHistory } from "react-router-dom";
import IconButton from '@material-ui/core/IconButton';
import AppContext from '../context/index';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,

  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  logo: {
    maxHeight: '100px',
  },
  button: {
    color: 'white',
  },
}));

export default function TopBar() {
  const classes = useStyles();
  let history = useHistory();
  const { loggedIn, setLoggedIn } = useContext(AppContext);


  function HomeIcon(props) {
    return (
      <SvgIcon {...props}>
        <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
      </SvgIcon>
    );
  }

  /*
  useEffect(() => {
    console.log('context here: ', loggedIn);
}, [loggedIn]);
*/

  let handleClickLogOut = () => {
    localStorage.setItem("Name", "");
    localStorage.setItem("UserName", "");
    localStorage.setItem("SessionToken", "");
    localStorage.setItem("Profile", "");
    localStorage.setItem("Status", "");
    setLoggedIn(false);
    history.push("/login");

  }


  return (
    <Box display="flex">
      <AppBar position="static">
        <Toolbar>
          <Link color="inherit" to="/">
            <img src={logo} className={classes.logo} alt="Logo" />
          </Link>
          <Typography variant="h6" className={classes.title}>
            Observatorio Pyme
          </Typography>
          {
            (localStorage.getItem("Status") == "ACTIVO")//Si esta autenticado, muestro botón de Home
              ?
              (
                <Link color="inherit" to="/listadoEncuestas">
                  <HomeIcon style={{ color: "white", fontSize: 25 }} ></HomeIcon>
                </Link>
              )
              :
              (
                <React.Fragment />
              )
          }
          <AccountCircle />
          {
            (localStorage.getItem("Status") == "ACTIVO")//Si esta autenticado, muestro nombre y perfil
              ?
              (
                <React.Fragment>
                  <div style={{ display: 'block', width: '8%' }}>
                    <div position="relative">{localStorage.getItem("Name")}</div>
                    <div style={{ position: "relative" }}>{localStorage.getItem("Profile")}</div>
                  </div>
                  <IconButton onClick={handleClickLogOut}>
                    <ExitToAppIcon
                      style={{ color: "white", fontSize: 25 }}
                    />
                  </IconButton>
                </React.Fragment>
              )
              :
              (
                <Link color="inherit" to="/login">
                  <Button className={classes.button}>Login</Button>
                </Link>
              )
          }
        </Toolbar>
      </AppBar>
    </Box>
  );
}