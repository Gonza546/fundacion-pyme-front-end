import React from 'react';
import InputSeleccionMultiple from './Inputs/InputSeleccionMultiple';
import InputSeleccionSimple from './Inputs/InputSeleccionSimple';
import InputCampoNumerico from './Inputs/InputCampoNumerico';
import InputCampoTexto from './Inputs/InputCampoTexto';
import InputCampoArchivo from './Inputs/InputCampoArchivo';

function RenderTexto(propiedades) {
    return <InputCampoTexto CompProps={propiedades} />
}

function RenderArchivo(propiedades) {
    return <InputCampoArchivo CompProps={propiedades} />
}

function RenderNumeros(propiedades) {
    return <InputCampoNumerico CompProps={propiedades} />
}

function RenderSeleccionSimple(propiedades) {
    return <InputSeleccionSimple CompProps={propiedades} />;
}

function RenderSeleccionMultiple(propiedades) {
    return <InputSeleccionMultiple CompProps={propiedades} />;
}

class RenderRta extends React.Component {



    render() {
        return (
            <React.Fragment>
                {
                    (this.props.tipo === "texto")
                        ?
                        (
                            RenderTexto(this.props)
                        )
                        :
                        (
                            (this.props.tipo === "numerica")
                                ?
                                (
                                    RenderNumeros(this.props)
                                )
                                :
                                (
                                    (this.props.tipo === "simple")
                                        ?
                                        (
                                            RenderSeleccionSimple(this.props)
                                        )
                                        :
                                        (
                                            (this.props.tipo === "multiple")
                                                ?
                                                (
                                                    RenderSeleccionMultiple(this.props)
                                                    //'Multiple'
                                                )
                                                :
                                                (
                                                    (this.props.tipo === "archivo")
                                                        ?
                                                        (
                                                            RenderArchivo(this.props)
                                                            
                                                        )
                                                        :
                                                        (
                                                            (this.props.tipo === "agrupada")
                                                                ?
                                                                (
                                                                    //RenderArchivo(this.props)
                                                                    'Agrupada'
                                                                )
                                                                :
                                                                (
                                                                    'Tipo no valido, revisar JSON'
                                                                )
                                                        )
                                                )
                                        )
                                )
                        )
                }
            </React.Fragment>
        );
    }
}


export default RenderRta;