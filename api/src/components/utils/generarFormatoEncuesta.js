import React from 'react';

export function crearFormatoEncuesta(datosBD, filtro) {
  var encuestas = [];
  var encuestaSeleccionada;
  var idEncuestaParticular = "id-";

  for (var i = 0; i < datosBD.length; i++) {
    var encuestasRelacionadas = [];
    for (var j = 0; j < datosBD.length; j++) {
      if (datosBD[j].titulo === datosBD[i].titulo) {
        encuestaSeleccionada =
        {
          idpyme: datosBD[j].idEncuesta,
          titulo: datosBD[j].titulo,
          pyme: datosBD[j].pyme,
          statusEncuesta: datosBD[j].status
        };
        encuestasRelacionadas.push(encuestaSeleccionada);
      }
    }
    if (encuestas.length === 0 || (encuestas[encuestas.length - 1].titulo !== encuestasRelacionadas[0].titulo)) {
      if (filtro != undefined) {
        if (datosBD[i].status == filtro) {
          encuestas.push({ idEncuesta: datosBD[i].idEncuesta, titulo: datosBD[i].titulo, pymes: encuestasRelacionadas })
        }
      }else{
        encuestas.push({ idEncuesta: datosBD[i].idEncuesta, titulo: datosBD[i].titulo, pymes: encuestasRelacionadas })
      }
    }
  }
  return encuestas;
}