
export function convertirCadena(cadena) {
  var cadenaConvertida = [];
  for (var i = 0; i < cadena.length; i++) {
    var valorConvertido;
    if (i === 0 || i === cadena.length - 1) {
      if (i === 0) {
        valorConvertido = cadena[i].substring(1);
        cadenaConvertida.push(valorConvertido);
      } else {
        valorConvertido = cadena[i];
        valorConvertido = valorConvertido.substring(0, valorConvertido.length - 1);
        cadenaConvertida.push(valorConvertido);
      }
    } else {
      cadenaConvertida.push(cadena[i]);
    }
  }
  return cadenaConvertida;
}

export function crearFormatoPreguntas(datosPreguntas, datosRespuestas, datosComentarios) {
  var preguntas = [];
  var pregunta;
  var numeroPregunta = 1;
  var comentarioSeleccionado;
  let respuesta;
  let respuestaOriginal;

  for (var i = 0; i < datosPreguntas.length; i++) {
    var respuestas = [];
    var respuestaActualizada = "";
    var comentariosRelacionados = [];
    for (var l = 0; l < datosComentarios.length; l++) {
      if (datosComentarios[l].idPregunta === datosPreguntas[i].idPregunta) {
        comentarioSeleccionado = {
          idComentario: datosComentarios[l].idComentario,
          idPregunta: datosComentarios[l].idPregunta,
          idUsuario: datosComentarios[l].idUsuario,
          comentario: datosComentarios[l].comentario,
          fecha: datosComentarios[l].createdAt.substring(0, 10),
          usuario: datosComentarios[l].usuario.nombre,
          rol: datosComentarios[l].usuario.rol
        };
        comentariosRelacionados.push(comentarioSeleccionado);
      }
    }
    //En este for separo las encuestas por pregunta y las convierto en el formato esperado.
    for (var j = 0; j < datosRespuestas.length; j++) {
      if (datosPreguntas[i].idPregunta === datosRespuestas[j].idPregunta) {
        if (datosPreguntas[i].type === "multiple") {
          var respuestasPosibles = datosRespuestas[j].respuesta;
          var cadenaRespuestas = JSON.stringify(respuestasPosibles.split(","));
          //cadenaRespuestas = convertirCadena(cadenaRespuestas);
          respuesta = {
            respuestaOriginal: generarObjetoRta(datosPreguntas[i].valoresPosibles, cadenaRespuestas),
            esActualizada: datosRespuestas[j].esActualizada
          };
        } else {
          respuesta = {
            respuestaOriginal: datosRespuestas[j].respuesta,
            esActualizada: datosRespuestas[j].esActualizada
          }
        }
        respuestas.push(respuesta);
      }

    }
    //En este for me guardo todas las respuestas que junte.
    for (var k = 0; k < respuestas.length; k++) {
      if (respuestas[k].esActualizada === false) {
        respuestaOriginal = respuestas[k].respuestaOriginal;
      }
      else { respuestaActualizada = respuestas[k].respuestaOriginal }
    }
    //aca convierto y me guardo los valores posibles al formato que espero.
    if (datosPreguntas[i].valoresPosibles !== null) {
      var valoresPosibles = JSON.stringify(datosPreguntas[i].valoresPosibles);
      var cadenaValores = valoresPosibles.split(",");
      cadenaValores = convertirCadena(cadenaValores);
      valoresPosibles = cadenaValores;
    } else { valoresPosibles = datosPreguntas[i].valoresPosibles }
    //creo el formato esperado.
    pregunta = {
      idEncuesta: datosPreguntas[i].idEncuesta,
      idPregunta: datosPreguntas[i].idPregunta,
      numeroPregunta: numeroPregunta,
      idSeccion: datosPreguntas[i].idSeccion,
      titulo: datosPreguntas[i].titulo,
      mandatory: datosPreguntas[i].mandatory,
      status: datosPreguntas[i].status,
      type: datosPreguntas[i].type,
      valoresPosibles: valoresPosibles,
      rtaOriginal: respuestaOriginal,
      rtaActualizada: respuestaActualizada,
      history: { comentariosRelacionados }
    };
    preguntas.push(pregunta);
    numeroPregunta++;
  }
  return preguntas;
}

function generarObjetoRta(valoresPosibles, rtas) {
  var out = {};
  var valoresPosiblesArray = Array.from(valoresPosibles.split(','));
  valoresPosiblesArray.map((valor) => {
    out[valor] = rtas.includes(valor) ? true : false;
  })
  return out;
}