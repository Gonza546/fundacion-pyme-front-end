import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import ListarEncuestas from './listadoEncuestas'
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Filtro from './Filtro';
import UserProfile from './UserProfile';


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        '& > *': {
            margin: theme.spacing(1),
        },
    },
}));


export default function ListarEncuestasFiltro(props) {

    const [filterApplied, setFilterApplied] = React.useState(false);
    const classes = useStyles();

    function handleChange(val) {
        setFilterApplied(val);
    }

    let handleClickTodas = () => {
        Filtro.setValorFiltrado('Todo');
        setFilterApplied(!filterApplied)

    }

    let handleClickAprobacionPendiente = () => {
        Filtro.setValorFiltrado('Pendiente Aprobador');
        setFilterApplied(!filterApplied)
    }

    let handleClickPendienteAnalista = () => {
        Filtro.setValorFiltrado('Pendiente Analista');
        setFilterApplied(!filterApplied)
    }

    let handleClickAprobadas = () => {
        Filtro.setValorFiltrado('Aprobada');
        setFilterApplied(!filterApplied)
    }


    return (
        <React.Fragment>
            {
                (localStorage.getItem("Profile") === 'Aprobador')
                    ?
                    (
                        <React.Fragment>
                            <Box margin={1} className={classes.root}>
                                <ButtonGroup variant="text" color="primary" aria-label="text primary button group">
                                    <Button onClick={handleClickTodas}>Todas</Button>
                                    <Button onClick={handleClickAprobacionPendiente}>Pendientes Aprobador</Button>
                                    <Button onClick={handleClickPendienteAnalista}>Pendientes Analista de Campo</Button>
                                    <Button onClick={handleClickAprobadas}>Aprobadas</Button>
                                </ButtonGroup>
                            </Box>
                            <ListarEncuestas filter={filterApplied}></ListarEncuestas>
                        </React.Fragment>
                    )
                    :
                    (
                        <ListarEncuestas />
                    )
            }
        </React.Fragment>
    );


}