import encuestas from '../assets/encuestas.json'

export function devolverEncuesta(idEncuesta, idpyme) {
  return encuestas.filter(encu => encu.idEncuesta === idEncuesta).map(enc =>
    ({
      idEncuesta: enc.idEncuesta,
      tituloEncuesta: enc.tituloEncuesta,
      pyme: (enc.pymes.filter(p => p.idpyme === idpyme)[0])
    })
  )[0];
}

//aca falta filtrar por estado!
export function devolverListadoEncuestas2(status) {
  return encuestas
}

export function devolverListadoEncuestasPorEstados(status) {
  if (status === 'Todo') {
    return encuestas
  }
  else {
    return encuestas.filter(e => e).map(enc =>
      ({
        idEncuesta: enc.idEncuesta,
        tituloEncuesta: enc.tituloEncuesta,
        pymes: (enc.pymes.filter(p => p.statusEncuesta === status))
      })
    )
  }
}


