import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import { Link } from "react-router-dom";
import Button from '@material-ui/core/Button';
import filtro from './Filtro';
import { green, grey, red } from '@material-ui/core/colors';
import CheckIcon from '@material-ui/icons/Check';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { crearFormatoEncuesta } from "./utils/generarFormatoEncuesta";
import { listarEncuesta, listarPreguntasporEncuesta } from "../controllers/appController";
import { useHistory } from "react-router-dom";

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});

function Row(props) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();



  return (
    <React.Fragment>
      <Box margin={1}>
        <Typography variant="h5" gutterBottom component="div">
          {row.titulo} ({row.pymes.length})
          <IconButton aria-label="ver detalles" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </Typography>
      </Box>

      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Table size="small" aria-label="comentarios">
                <TableHead>
                  <TableRow>
                    <TableCell>Nombre Pyme</TableCell>
                    <TableCell>Estado encuesta</TableCell>
                    <TableCell>Acceder</TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                </TableHead>


                <TableBody>
                  {row.pymes.map((pyme) => (
                    <TableRow key={pyme.idpyme}>
                      <TableCell component="th" scope="row">{pyme.pyme}</TableCell>
                      <TableCell>{pyme.statusEncuesta}</TableCell>
                      {//<TableCell>{row.validadas}</TableCell>
                      }
                      <TableCell>
                        <Link color="inherit" to={{
                          pathname: "/validarEncuesta",
                          state: {
                            idEncuesta: pyme.idpyme,
                            pyme: pyme.pyme,
                            titulo: pyme.titulo,
                            status:pyme.statusEncuesta
                          }
                        }}>
                          {
                            (pyme.statusEncuesta === 'Aprobada' || pyme.statusEncuesta === 'Pendiente Analista')
                              ?
                              (
                                <Button className={classes.button} style={{ color: green[500] }} >
                                  <VisibilityIcon fontSize="large"></VisibilityIcon>
                                </Button>
                              )
                              :
                              (
                                <Button className={classes.button} style={{ color: grey[500] }} >
                                  <CheckIcon fontSize="large"></CheckIcon>
                                </Button>
                              )
                          }
                        </Link>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}



Row.propTypes = {
  row: PropTypes.shape({
    idEncuesta: PropTypes.number.isRequired,
    titulo: PropTypes.string.isRequired,
    pymes: PropTypes.arrayOf(
      PropTypes.shape({
        idpyme: PropTypes.string.isRequired,
        pyme: PropTypes.string.isRequired,
      }),
    ).isRequired,
  }).isRequired,
};


export default function ListarEncuestas({ filter }) {
  const [reloadData, setReloadData] = React.useState(true);
  const [listaEncuestas = [], setListaEncuestas] = React.useState();

  let data;
  let history = useHistory();

  useEffect(() => {
    if (localStorage.getItem("SessionToken") == '') {
      history.push("/");
    }
    else {
      getEncuestas();
    }
  }, [filter]);


  const getEncuestas = async function () {
    var filter;
    filter = localStorage.getItem("Profile") === 'Aprobador' ? filtro.getValorFiltrado() : 'Pendiente Analista';
    data = await listarEncuesta(filter);
    if (data.length !== 0) {
      setListaEncuestas(crearFormatoEncuesta(data));
    }
  }

  return (
    <React.Fragment>
      <TableContainer component={Paper}>
        <Table aria-label="collapsible table">
          <TableBody>

            {
              listaEncuestas.filter(e => (e.pymes.length > 0)).map((row) => (
                <Row key={row.idEncuesta} row={row} />
              ))
            }
          </TableBody>
        </Table>
      </TableContainer>
    </React.Fragment>
  );
}
