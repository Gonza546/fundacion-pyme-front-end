var UserProfile = (function () {

    if (localStorage.getItem("Name") == undefined) {
        localStorage.setItem("Name", "");
        localStorage.setItem("UserName", "");
        localStorage.setItem("SessionToken", "");
        localStorage.setItem("Profile", "");
        localStorage.setItem("Status", "");
    }

    return {}

})();

export default UserProfile;