import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { listarPreguntasporEncuesta, listarRespuestas, buscarComentarios, encuestaAPendienteAprobador, encuestaAprobada, encuestaARevision } from "../controllers/appController";
import { crearFormatoPreguntas } from "./utils/generarFormatoPregunta";
import MostrarPregunta from "./pregunta";
import { useHistory } from "react-router-dom";
import SimpleModal from '../components/modal';


const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
  Button: {
    margin: 200,
  },
});


export default function ValidarEncuesta(props, { filter }) {

  const [openModal, setOpenModal] = React.useState(false);
  const [title, setTitle] = React.useState('');
  const [body, setBody] = React.useState('');


  let handleClickAAprobacion = () => {
    encuestaAAprobacionApi(enc);
  }

  let handleClickRevision = () => {
    encuestaARevisionApi(enc);
  }

  let handleClickAprobada = () => {
    encuestaAprobadaApi(enc);
  }

  let handleOkButton = () => {
    history.push("/listadoEncuestas");
  }



  const encuestaAAprobacionApi = async function (idEncuesta) {
    let data = await encuestaAPendienteAprobador(idEncuesta);
    //console.log("data: " + data);
    if (data != undefined && data.status == 200) {
      openModalMsg("Éxito", "Encuesta enviada para Aprobación");
    }
    else {
      openModalMsg("Error", "Existen preguntas pendientes por el analista");
    }
  }

  const encuestaAprobadaApi = async function (idEncuesta) {
    let data = await encuestaAprobada(idEncuesta);

    if (data != undefined && data.status == 200) {
      openModalMsg("Éxito", "Encuesta Aprobada");
    }
    else {
      openModalMsg("Error", "Existen preguntas pendientes de aprobación");
    }
  }

  const encuestaARevisionApi = async function (idEncuesta) {
    let data = await encuestaARevision(idEncuesta);

    if (data != undefined && data.status == 200) {
      openModalMsg("Éxito", "Encuesta enviada a revisión");
    }
    else {
      openModalMsg("Error", data.message);
    }
  }

  let openModalMsg = (title, body) => {
    setTitle(title);
    setBody(body);
    setOpenModal(true);
  }


  let handleClickVolver = () => {
    history.push("/listadoEncuestas");
  }

  const enc = props.location.state.idEncuesta;
  const tituloEncuesta = props.location.state.titulo;
  const pyme = props.location.state.pyme;
  const status = props.location.state.status;

  const [listaPreguntas = [], setListaPreguntas] = React.useState();
  let data;
  let data2;
  let data3;
  let history = useHistory();
  const [reload, setReload] = useState(false);


  useEffect(() => {
    if (localStorage.getItem("SessionToken") == '') {
      history.push("/");
    }
    else {
      getPreguntas();
    }
  }, [reload]);


  const getPreguntas = async function () {
    data = await listarPreguntasporEncuesta(enc);
    data2 = await listarRespuestas(enc);
    data3 = await buscarComentarios(enc);
    if (data.length !== 0) {
      setListaPreguntas(crearFormatoPreguntas(data, data2, data3));
    }
  }

  return (
    <React.Fragment>
      <TableContainer component={Paper}>
        <Box margin={1}>
          <Typography variant="h5" gutterBottom component="div">
            Encuesta: {tituloEncuesta}
          </Typography>
        </Box>
        <Box margin={1}>
          <Typography variant="h7" gutterBottom component="div">
            Pyme: {pyme}
          </Typography>
        </Box>
        <Table aria-label="collapsible table">
          <TableBody>
            {
              listaPreguntas.map((preg) => (
                <MostrarPregunta
                  unaPregunta={preg}
                  onChange={setReload}
                  reload={reload}
                  statusEncuesta={status}
                />
              ))
            }
          </TableBody>
        </Table>
      </TableContainer>

      <footer className='App-footer'>
        {
          (localStorage.getItem("Profile") === 'Aprobador')
            ?
            (
              (status === 'Pendiente Aprobador')
                ?
                (
                  <React.Fragment>
                    <span style={{ marginLeft: 50 }}>
                      <Button onClick={handleClickVolver} variant="contained" color="primary" size="large">Volver</Button>
                    </span>
                    <span style={{ float: "right" }}>
                      <Button onClick={handleClickRevision} variant="contained" color="primary" size="large" >Enviar a Revisión</Button>
                      <span style={{ marginLeft: 30, marginRight: 200 }}>
                        <Button onClick={handleClickAprobada} variant="contained" color="primary" size="large">Aprobar</Button>
                      </span>
                    </span>
                  </React.Fragment>
                )
                :
                (
                  <React.Fragment>
                    <span style={{ marginLeft: 50 }}>
                      <Button onClick={handleClickVolver} variant="contained" color="primary" size="large">Volver</Button>
                    </span>
                  </React.Fragment>
                )
            )
            :
            (
              <React.Fragment>
                <Button onClick={handleClickVolver} variant="contained" color="primary" size="small" >Volver</Button>
                <span style={{ float: "right" }}>
                  <span style={{ marginLeft: 30, marginRight: 200 }}>
                    <Button onClick={handleClickAAprobacion} variant="contained" color="primary" size="small" >Enviar a Aprobación</Button>
                  </span>
                </span>
              </React.Fragment>
            )
        }
      </footer>
      <SimpleModal
        open={openModal}
        title={title}
        body={body}
        visibleCancelButton={false}
        okButtonAction={handleOkButton}
      />
    </React.Fragment>
  );
}