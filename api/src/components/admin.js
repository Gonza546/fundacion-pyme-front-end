import React from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import { Grid, MenuItem } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { addUser } from "../controllers/appController";
import Alert from '../components/alert';
import { useHistory } from "react-router-dom";
import UserProfile from './UserProfile';

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
      marginTop: '25px',
    },
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const roles = [
  {
    value: 'Aprobador',
    label: 'Aprobador',
  },
  {
    value: 'Analista',
    label: 'Analista',

  },

];

export default function FormPropsTextFields() {
  const classes = useStyles();
  const [rol, setRol] = React.useState('Analista');
  const [showSuccessMessage, setShowSuccessMessage] = React.useState(false);
  let history = useHistory();

  if (localStorage.getItem("SessionToken") == '') {
    history.push("/")
  }

  const handleChange = (event) => {
    setRol(event.target.value);
  };

  const addUsers = e => {
    e.preventDefault();
    var fullName = document.getElementById('outlined-name-required').value;
    var email = document.getElementById('outlined-email-required').value;
    var pass = document.getElementById('outlined-password-input').value;
    addUserApi(fullName, rol, email, pass);

  };

  const addUserApi = async function (fullName, rol, email, pass) {
    let data = await addUser(fullName, rol, email, pass);
    //console.log("data: " + data);
    if (data != undefined && data.length !== 0 && data.status == 200) {
      setShowSuccessMessage(true);
    } else {
      setShowSuccessMessage(false);
    }
  }


  return (
    <Grid>
      <Grid item xs={12} container justify="center" alignItems="center">
        <Button variant="contained" color="primary" onClick={() => { history.push("/admin") }}>
          Alta de usuario
			</Button>

        <Button variant="contained" color="secondary" onClick={() => { history.push("/editarusuario") }}>
          Lista de usuarios
			</Button>
      </Grid>
      <Grid item xs={12} container justify="center" alignItems="center">
        <form className={classes.root} noValidate autoComplete="off">
          <div>
            <h1>Nuevo usuario</h1>
            <Box display="flex" justifyContent="center" flexWrap="nowrap">
              <TextField
                required
                id="outlined-name-required"
                label="Nombre Completo"
                defaultValue=""
                variant="outlined"
              />
              <TextField
                id="outlined-select-rol"
                select
                label="Rol"
                value={rol}
                onChange={handleChange}
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
              >
                {roles.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>

            </Box>
            <Box display="flex" justifyContent="center" flexWrap="nowrap">
              <TextField
                required
                id="outlined-email-required"
                label="Email"
                defaultValue=""
                variant="outlined"
              />
              <TextField
                required
                id="outlined-password-input"
                label="Contraseña"
                autoComplete="current-password"
                type="password"
                defaultValue=""
                variant="outlined"
              />
            </Box>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={addUsers}
            >
              Registrar
          </Button>
            {
              (showSuccessMessage)
                ?
                (
                  <Alert CompProps='Usuario creado con éxito' type='success' />
                )
                :
                (
                  <React.Fragment />
                )
            }
          </div>
        </form>
      </Grid>
    </Grid>
  );
}
