import React, {useState,useEffect} from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";

export default function MultilineText(props) {


  const [visible,setVisible]=React.useState(true);
  const useStyles = makeStyles((theme) => ({
    root: {
      "& .MuiTextField-root": {
        margin: theme.spacing(1),
        width: 400
      }
    }
  }));

  useEffect(() => {

    verificarVisibilidad()
  });

  const handleInputChange = (e) => {
    props.callbackFromParent(e.target.value)
  }

  function verificarVisibilidad(){
    if(props.visible==false){
      setVisible(false);
    }
  }
  
  const classes = useStyles();


  return (
<React.Fragment>
    {(visible===true)?
    <form className={classes.root}>
      <div>
        <TextField
          key={props.key}
          id={props.id}
          multiline rows={3}
          placeholder="Agregar Comentario"
          variant="outlined"
          margin='normal'
          value={props.comentario}
          onChange={handleInputChange}
          visible={props.visible}
        />
      </div>
    </form>
    :null}
    </React.Fragment>
  );
}