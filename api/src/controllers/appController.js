import urlWebServices from '../controllers/webServices.js';
import UserProfile from '../components/UserProfile';

let origin = 'http://localhost:3000';

export const listarEncuesta = async function (filtro) {
    let url = urlWebServices.listarEncuestas;
    const formData = new URLSearchParams();
    formData.append('status', filtro);


    try {
        let response = await fetch(url, {
            method: 'POST',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            },
            body: formData
        });

        const data = await response.json()
        return data;
    }
    catch (error) {
        console.log("error", error);
    };
}

export const listarPreguntasporEncuesta = async function (idEncuesta) {
    let url = urlWebServices.listarPreguntasporEncuesta;
    const formData = new URLSearchParams();
    formData.append('idEncuesta', idEncuesta);
    try {
        let response = await fetch(url, {
            method: 'POST',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            },
            body: formData// => esto va si yo le mando datos al servicio en el body
        });
        let data = await response.json();
        return data;

    }
    catch (error) {
        console.log("error", error);
    }
}
export const listarRespuestas = async function (idEncuesta) {
    let url = urlWebServices.buscarRespuestaporidEncuesta;
    const formData = new URLSearchParams();
    formData.append('idEncuesta', idEncuesta);
    try {
        let response = await fetch(url, {
            method: 'POST',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            },
            body: formData
        });
        let data = await response.json();
        return data;
    }
    catch (error) {
        console.log("error", error);
    }
}

export const buscarComentarios = async function (idEncuesta) {
    let url = urlWebServices.buscarComentariosPoridEncuesta;
    const formData = new URLSearchParams();
    formData.append('idEncuesta', idEncuesta);
    try {
        let response = await fetch(url, {
            method: 'POST',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            },
            body: formData
        });
        let data = await response.json();
        return data;
    }
    catch (error) {
        console.log("error", error);
    }
}

export const buscarComentariosPoridPregunta = async function (idPregunta) {
    let url = urlWebServices.buscarComentariosPorPregunta;
    const formData = new URLSearchParams();
    formData.append('idPregunta', idPregunta);
    //console.log("este es el fomdata:",formData);
    try {
        let response = await fetch(url, {
            method: 'POST',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            },
            body: formData
        });
        //console.log("response",response);
        let data = await response.json();
        //console.log("QUIERO VER LA DATA",data);
        return data;
    }
    catch (error) {
        console.log("error", error);
    }
}

export const loginUser = async function (email, password) {
    let url = urlWebServices.loginUser;
    var formData = new URLSearchParams();
    formData.append('email', email);
    formData.append('password', password);

    try {
        let response = await fetch(url, {
            method: 'POST',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: formData
        });

        //let rdo = response.status;
        //console.log("response",response);
        let data = await response.json();
        //console.log("jsonresponse",data);
        return data;
    }
    catch (error) {
        console.log("error", error);
    }
}

export const findUser = async function (email) {
    let url = urlWebServices.buscarUser;
    const formData = new URLSearchParams();
    formData.append('email', email);
    try {
        let response = await fetch(url, {
            method: 'POST',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            },
            body: formData
        });
        let data = await response.json();
        return data;
    }
    catch (error) {
        console.log("error", error);
    }
}

export const crearComentario = async function (comentario) {
    let url = urlWebServices.crearComentario;
    const formData = new URLSearchParams();
    formData.append('comentario', comentario.comentario);
    formData.append('idPregunta', comentario.idPregunta);
    formData.append('idUsuario', comentario.idUsuario);
    try {
        let response = await fetch(url, {
            method: 'POST',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            },
            body: formData
        });
        let data = await response.json();
        return data;
    }
    catch (error) {
        console.log("error", error);
    }
}

export const preguntaAprobada = async function (idPregunta) {
    let url = urlWebServices.preguntaAprobada;
    const formData = new URLSearchParams();
    formData.append('idPregunta', idPregunta);
    try {
        let response = await fetch(url, {
            method: 'PATCH',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            },
            body: formData
        });
        let data = await response.json();
        return data;
    }
    catch (error) {
        console.log("error", error);
    }
}

export const preguntaDevolver = async function (idPregunta) {
    let url = urlWebServices.preguntaDevolver;
    const formData = new URLSearchParams();
    formData.append('idPregunta', idPregunta);
    try {
        let response = await fetch(url, {
            method: 'PATCH',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            },
            body: formData
        });
        let data = await response.json();
        return data;
    }
    catch (error) {
        console.log("error", error);
    }
}

export const addUser = async function (fullName, rol, email, pass) {
    let url = urlWebServices.addUser;
    var formData = new URLSearchParams();
    formData.append('fullname', fullName);
    formData.append('email', email);
    formData.append('password', pass);
    formData.append('role', rol);

    try {
        let response = await fetch(url, {
            method: 'POST',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            },
            body: formData
        });

        //let rdo = response.status;
        //console.log("response",response);
        let data = await response.json();
        //console.log("jsonresponse",data);
        return data;
    }
    catch (error) {
        console.log("error", error);
    }
}

export const getUsers = async function () {
    let url = urlWebServices.getUsers;

    try {
        let response = await fetch(url, {
            method: 'GET',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            }
        });

        //let rdo = response.status;
        //console.log("response",response);
        let data = await response.json();
        //console.log("jsonresponse",data);
        return data;
    }
    catch (error) {
        console.log("error", error);
    }
}

export const deleteUsers = async function (users) {
    let url = urlWebServices.deleteUsers;
    var formData = new URLSearchParams();
    users.map((user) => {
        formData.append('email', user);
    })
    try {
        let response = await fetch(url, {
            method: 'POST',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            },
            body: formData
        });

        //let rdo = response.status;
        //console.log("response",response);
        let data = await response.json();
        //console.log("jsonresponse",data);
        return data;
    }
    catch (error) {
        console.log("error", error);
    }
}

export const rechazarPreguntaPoridPregunta = async function (idPregunta) {
    let url = urlWebServices.preguntaRechazar;
    const formData = new URLSearchParams();
    formData.append('idPregunta', idPregunta);
    try {
        let response = await fetch(url, {
            method: 'PATCH',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            },
            body: formData
        });
        let data = await response.json();
        console.log(data);
        return data;
    }
    catch (error) {
        console.log("error", error);
    }
}

export const encuestaAPendienteAprobador = async function (idEncuesta) {
    let url = urlWebServices.encuestaAPendienteAprobador;
    const formData = new URLSearchParams();
    formData.append('idEncuesta', idEncuesta);
    try {
        let response = await fetch(url, {
            method: 'POST',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            },
            body: formData
        });
        let data = await response.json();
        return data;
    }
    catch (error) {
        console.log("error", error);
    }
}

export const encuestaAprobada = async function (idEncuesta) {
    let url = urlWebServices.encuestaAprobada;
    const formData = new URLSearchParams();
    formData.append('idEncuesta', idEncuesta);
    try {
        let response = await fetch(url, {
            method: 'POST',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            },
            body: formData
        });
        let data = await response.json();
        return data;
    }
    catch (error) {
        console.log("error", error);
    }
}



export const actualizarRespuesta = async function (respuesta) {
    let url = urlWebServices.actualizarRespuesta;
    const formData = new URLSearchParams();
    formData.append('rtaActualizada', respuesta.rtaActualizada);
    //formData.append('respuestaVieja',respuesta.rtaOriginal);//esta linea se puede sacar cuando actualice la tabla con los datos de gabi.
    formData.append('idPregunta', respuesta.idPregunta);
    try {
        let response = await fetch(url, {
            method: 'PATCH',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': origin,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            },
            body: formData
        });
        let data = await response.json();
        return data;
    }
    catch (error) {
        console.log("error", error);
    }
}

export const encuestaARevision = async function (idEncuesta) {
    let url = urlWebServices.encuestaARevision;
    const formData = new URLSearchParams();
    formData.append('idEncuesta', idEncuesta);
    try {
        let response = await fetch(url, {
            method: 'POST',
            mode: "cors",
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Origin': 'http://localhost:3000',
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem("SessionToken")
            },
            body: formData
        });
        let data = await response.json();
        return data;
    }
    catch (error) {
        console.log("error", error);
    }
}