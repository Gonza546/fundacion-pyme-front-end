export const urlApi = "http://localhost:4000/";

const urlWebServices = {

    listarEncuestas: urlApi + "api/encuestas/list",
    listarEncuestasPorId: urlApi + "api/encuestas/find",
    listarPreguntasporEncuesta: urlApi + "api/preguntas/find",
    buscarRespuestaporidEncuesta: urlApi + "api/respuestas/find",
    buscarComentariosPoridEncuesta: urlApi + "api/comentarios/find",
    buscarComentariosPorPregunta: urlApi + "api/comentarios/findIdPreg",
    buscarUser: urlApi + "api/user/find",
    crearComentario: urlApi + "api/comentarios/create",
    preguntaAprobada: urlApi + "api/preguntas/aprobar",
    preguntaRechazar: urlApi + "api/preguntas/rechazar",
    preguntaDevolver: urlApi + "api/preguntas/devolver",    
    encuestaAPendienteAprobador : urlApi + "api/encuestas/pendienteAprobador",
    encuestaAprobada: urlApi + "api/encuestas/Aprobada",
    actualizarRespuesta: urlApi + "api/respuestas/update",
    encuestaARevision: urlApi + "api/encuestas/pendienteAnalista",
    //Login
    loginUser: urlApi + "api/login",

    //Users
    addUser: urlApi + "api/user/create",
    getUsers: urlApi + "api/user/list",
    deleteUsers: urlApi + "api/user/delete"
}

export default urlWebServices;