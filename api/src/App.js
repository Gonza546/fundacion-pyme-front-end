import React from 'react';
import './assets/css/App.css';
import TopBar from './components/TopBar';
import { Route, Switch, Redirect } from 'react-router-dom';
import Login from './components/Login';
import ValidarEncuesta from './components/encuesta';
import ListarEncuestasFiltro from './components/BarFiltrado'
import AdminPanel from './components/admin'
import Container from '@material-ui/core/Container';
import ListaUsuarios from './components/ListaUsuarios';

function App() {
  const [loggedIn, setLoggedIn] = React.useState(false);

  function handleChange(newValue) {
    setLoggedIn(newValue);
  }


  return (

    <Container maxWidth="false">

      <TopBar></TopBar>
      <Switch>
        <Route exact path="/"> <Redirect to="/login" />  </Route>
        <Route path="/login">
          <Login onChange={handleChange}></Login>
        </Route>
        <Route path="/listadoEncuestas" component={ListarEncuestasFiltro}></Route>
        <Route path="/validarEncuesta" component={ValidarEncuesta} ></Route>
        <Route path="/admin" component={AdminPanel} ></Route>
        <Route path="/editarusuario" component={ListaUsuarios} ></Route>

      </Switch>

    </Container>

  );
}

export default App;
