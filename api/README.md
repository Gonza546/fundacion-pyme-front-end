	Frontend:	
1)	Ubicarse en la raiz del proj -->	cd api
2)	Instalar dependencias -->	npm install
3)	Iniciar Frontend -->	npm start


Usuarios:
1) Admin
usuario: administrador@grupo4.com
clave: administrador

2) Aprobador
usuario: aprobador@grupo4.com
clave: aprobador

3) Analista
usuario: analista@grupo4.com
clave: analista

Endpoint default para API Backend: http://localhost:4000/ (este valor puede cambiarse en la primer linea del archivo webServices.js dentro de la carpeta Controllers)
